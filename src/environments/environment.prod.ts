export const environment = {
  commerce_url: 'https://a-commerce.acuma.dev/commerces/',
  a_ccount_url: 'https://services.a-ccount.acuma.dev/api/',
  products_url: 'https://products.a-commerce.acuma.dev/api/',
  orders_url: 'https://orders.a-commerce.acuma.dev/api/',
  production: true
};
