import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RoutesGuard } from './guards/routes.guard'; 
import { LoggedGuard } from './guards/logged.guard'; 

import { MainViewComponent } from './views/main-view/main-view.component';
import { LoginViewComponent } from './views/login-view/login-view.component';
import { OrderViewComponent } from './views/order-view/order-view.component';
import { ProductViewComponent } from './views/product-view/product-view.component';
import { CommerceViewComponent } from './views/commerce-view/commerce-view.component';
import { ShareCommerceViewComponent } from './views/share-commerce-view/share-commerce-view.component';
import { LayoutBuilderViewComponent } from './views/layout-builder-view/layout-builder-view.component';

const routes: Routes = [
  { path: 'commerce/:commerce_id/build-layout', component: LayoutBuilderViewComponent, canActivate: [RoutesGuard] },
  { path: 'commerce/:commerce_id/products/:product_id', component: ProductViewComponent, canActivate: [RoutesGuard] },
  { path: 'commerce/:commerce_id/orders/:order_id', component:OrderViewComponent, canActivate: [RoutesGuard] },
  { path: 'commerce/:commerce_id/share', component: ShareCommerceViewComponent, canActivate: [RoutesGuard] },
  { path: 'commerce/:commerce_id', component: CommerceViewComponent, canActivate: [RoutesGuard] },
  { path: 'commerce', component: MainViewComponent, canActivate: [RoutesGuard] },
  { path: 'login', component: LoginViewComponent,  canActivate: [LoggedGuard] },
  { path: ''  , redirectTo: '/login', pathMatch: 'full' },
  { path: '**', redirectTo: '/login' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
