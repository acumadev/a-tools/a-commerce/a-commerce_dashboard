import { HttpClient } from '@angular/common/http';
import { Injectable, EventEmitter } from '@angular/core';
import { map } from "rxjs/operators";

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  url = environment.orders_url
  order_updated = new EventEmitter();
  
  constructor(private http: HttpClient) { }

  getOrders(commerce_id) {
    return this.http.get(this.url+commerce_id+'/orders')
      .pipe(map(orders => {
      return orders;
    }));
  }

  getOrder(commerce_id, order_id) {
    return this.http.get(this.url+commerce_id+'/orders/'+order_id)
      .pipe(map(orders => {
      return orders;
    }));
  }

  updateOrder(commerce_id, order_id, data) {
    return this.http.patch(this.url+commerce_id+'/orders/'+order_id,{order:data})
      .pipe(map(order => {
      this.order_updated.emit()
      return order;
    }));
  }
}
