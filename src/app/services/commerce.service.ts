import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from "rxjs/operators";

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CommerceService {
  
  url = environment.a_ccount_url;
  commerce_added = new EventEmitter();

  constructor(private http: HttpClient) { }

  getAccountCommerce() {
    let user = JSON.parse(localStorage.getItem('acommerce-hash-user'))
    return this.http.get(this.url+'users/'+user.id+'/commerces')
      .pipe(map(commerces => {
      return commerces;
    }));
  }

  getCommerce(commerce_id) {
    let user = JSON.parse(localStorage.getItem('acommerce-hash-user'))
    return this.http.get(this.url+'users/'+user.id+'/commerces/'+commerce_id)
      .pipe(map(commerce => {
      return commerce;
    }));
  }

  createCommerce(data) {
    let user = JSON.parse(localStorage.getItem('acommerce-hash-user'))
    return this.http.post(this.url+'users/'+user.id+'/commerces',{"commerce": data})
      .pipe(map(commerce => {
      this.commerce_added.emit()
      return commerce;
    }));
  }

  updateCommerce(commerce_id, data) {
    let user = JSON.parse(localStorage.getItem('acommerce-hash-user'))
    return this.http.patch(this.url+'users/'+user.id+'/commerces/'+commerce_id,{"commerce": data})
      .pipe(map(commerce => {
      this.commerce_added.emit()
      return commerce;
    }));
  }
}
