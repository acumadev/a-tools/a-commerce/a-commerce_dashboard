import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from "rxjs/operators";

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  url = environment.a_ccount_url
  
  constructor(private http: HttpClient) { }

  loginAccount(data) {
    return this.http.post(this.url+'login', {credentials: data})
      .pipe(map(user => {
      return user;
    }));
  }

}
