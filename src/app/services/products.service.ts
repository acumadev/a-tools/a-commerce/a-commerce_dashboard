import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from "rxjs/operators";

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  url = environment.products_url
  product_added = new EventEmitter();

  constructor(private http: HttpClient) { }

  createProduct(commerce_id, data){
    return this.http.post(this.url+commerce_id+'/products', {product: data})
      .pipe(map(product => {
      this.product_added.emit()
      return product;
    }));
  }

  updateProduct(commerce_id, product_id, data){
    return this.http.patch(this.url+commerce_id+'/products/'+product_id, {product: data})
      .pipe(map(product => {
      return product;
    }));
  }

  getCommerceProducts(commerce_id){
    return this.http.get(this.url+commerce_id+'/products')
      .pipe(map(products => {
      return products;
    }));
  }
  getCommerceProduct(commerce_id, product_id){
    return this.http.get(this.url+commerce_id+'/products/'+product_id)
      .pipe(map(product => {
      return product;
    }));
  }
}
