import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'A-lytics-Dashboard';
  openSide = false

  constructor(
    private router: Router,
  ) {}

  logout(){
    localStorage.clear()
    sessionStorage.clear()
    this.router.navigate(['/login'])
  }
}