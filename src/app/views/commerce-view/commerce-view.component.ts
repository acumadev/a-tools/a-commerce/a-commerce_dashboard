import { Router } from '@angular/router'
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { OrderService } from '../../services/order.service';
import { ProductsService } from '../../services/products.service';
import { CommerceService } from '../../services/commerce.service';
import { AddProductComponent } from '../../dialogs/add-product/add-product.component';
import { EditCommerceDialogComponent } from '../../dialogs/edit-commerce-dialog/edit-commerce-dialog.component';

@Component({
  selector: 'app-commerce-view',
  templateUrl: './commerce-view.component.html',
  styleUrls: ['./commerce-view.component.scss']
})
export class CommerceViewComponent implements OnInit {

  membership
  production = false
  allow_add = false
  products = []
  orders = []
  constructor(
    private router: Router,
    public dialog: MatDialog,
    public location: Location,
    private order: OrderService,
    private route: ActivatedRoute,
    private product: ProductsService,
    private commerce: CommerceService,
  ) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.getProducts(params)
      this.order.getOrders(params.get('commerce_id'))
      .subscribe((data:any)=>{
        this.orders = data.data
      })
    });
    this.product.product_added
    .subscribe(()=>{
      this.route.paramMap.subscribe(params => {
        this.getProducts(params)
      })
    })
  }

  editCommerce() {
    this.route.paramMap.subscribe(params => {
      this.dialog.open(EditCommerceDialogComponent, {
        panelClass: 'popup_class',
        width: (window.innerWidth*.8)+'px',
        //height: (window.innerHeight*.8)+'px',
        data: {commerce_id: params.get('commerce_id')} 
      })
    });
  }

  getProducts(params){
    let commerce = JSON.parse(sessionStorage.getItem('a_commerce_data_info'))
    this.product.getCommerceProducts(params.get('commerce_id'))
    .subscribe((data:any)=>{
      this.products = data.data
      if(commerce.membership.no_products > this.products.length){
        this.allow_add = true
      }else{ this.allow_add = false }
    })
  }

  addProduct() {
    this.route.paramMap.subscribe(params => {
      this.dialog.open(AddProductComponent, {
        panelClass: 'popup_class',
        width: (window.innerWidth*.8)+'px',
        //height: (window.innerHeight*.8)+'px',
        data: {commerce_id: params.get('commerce_id')} 
      })
    });
  }

  goToLayoutCreation() {
    this.route.paramMap.subscribe(params => {
      this.router.navigate(['commerce',params.get('commerce_id'),'build-layout']);
    });
  }
  
  changeProd() {
    this.route.paramMap.subscribe(params => {
      this.commerce.updateCommerce(params.get('commerce_id'),{production: this.production})
      .subscribe((data:any)=>{
        console.log(data)
      })
    });
  } 
}
