import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LayoutBuilderViewComponent } from './layout-builder-view.component';

describe('LayoutBuilderViewComponent', () => {
  let component: LayoutBuilderViewComponent;
  let fixture: ComponentFixture<LayoutBuilderViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LayoutBuilderViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutBuilderViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
