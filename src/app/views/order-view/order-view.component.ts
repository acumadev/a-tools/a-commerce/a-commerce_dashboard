import { OrderService } from '../../services/order.service'; 
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-order-view',
  templateUrl: './order-view.component.html',
  styleUrls: ['./order-view.component.scss']
})
export class OrderViewComponent implements OnInit {

  order
  constructor(
    private route: ActivatedRoute,
    private orders: OrderService,
    public location: Location
  ) { }

  ngOnInit(): void {
    let location_data = history.state
    if(location_data.order){
      this.order = location_data.order
    }else{
      this.route.paramMap.subscribe(params => {
        this.orders.getOrder(params.get('commerce_id'),params.get('order_id'))
        .subscribe((data:any)=>{
          this.order = data.data
          console.log(this.order)
        })
      });
    }
    this.orders.order_updated
    .subscribe(()=>{
      this.route.paramMap.subscribe(params => {
        this.orders.getOrder(params.get('commerce_id'),params.get('order_id'))
        .subscribe((data:any)=>{
          this.order = data.data
          console.log(this.order)
        })
      });
    })
  }

  orderDelivered() {
    this.route.paramMap.subscribe(params => {
      this.orders.updateOrder(params.get('commerce_id'),params.get('order_id'),{collected:true})
      .subscribe((data:any)=>{
        console.log(data)
      })
    });
  }
}
