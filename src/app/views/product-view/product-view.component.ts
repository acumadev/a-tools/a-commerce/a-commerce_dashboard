import { ProductsService } from '../../services/products.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-product-view',
  templateUrl: './product-view.component.html',
  styleUrls: ['./product-view.component.scss']
})
export class ProductViewComponent implements OnInit {

  types = ["Comida", "Bebida", "Snack", "Dulces", "Papelería"]
  stock_units = ["kilogramo(s)","pieza(s)", "gramo(s)"]
  stock_unit = ""
  price_units = ["kilogramo","pieza", "gramo"]
  price_unit = ""
  selling_units = ["kilogramo(s)", "pieza(s)"]
  updating = false
  loading = false
  commerce_id
  product
  constructor(
    public location: Location, 
    private route: ActivatedRoute,
    private products: ProductsService   
  ) {}

  ngOnInit(){
    let location_data = history.state
    if(location_data.product){
      this.product = location_data.product
      this.product.price =  this.product.price.split(" ")
      this.product.stock =  this.product.stock.split(" ")
    }else{
      this.route.paramMap.subscribe(params => {
        this.products.getCommerceProduct(params.get('commerce_id'),params.get('product_id'))
        .subscribe((data:any)=>{
          this.product = data.data
          this.product.price =  this.product.price.split(" ")
          this.product.stock =  this.product.stock.split(" ")
        })
      });
    }
  }

  addCharacteristic(char){
    this.product.characteristics.push(char.value)
    char.value = ""
  }

  removeChar(char) {
    const index = this.product.characteristics.indexOf(char);
    if (index >= 0) {
      this.product.characteristics.splice(index, 1);
    }
  }

  noNegative(number) {
    // var numbers = /^[0-9]+$/;
    if(number.value < 0){
      number.value = ""
    }
  }

  addUnit(type) { 
    const index = this.product.units.indexOf(type.value);
    if (index < 0) {
      this.product.units.push(type.value);
    }
  }

  removeUnit(type): void {
    const index = this.product.units.indexOf(type);
    if (index >= 0) {
      this.product.units.splice(index, 1);
    }
  }

  uploadFile(event) {
    this.product.images = []
    for(let image of event.target.files){
      let reader = new FileReader();
      reader.readAsDataURL(image);
      reader.onload = () => this.product.images.push(reader.result);
    }
  }

  removeImage(image) {
    const index = this.product.images.indexOf(image);
    if (index >= 0) {
      this.product.images.splice(index, 1);
    }
  } 

  editProduct() {
    this.updating = true
    this.product.price = this.product.price.join(" ")
    this.product.stock = this.product.stock.join(" ")
    this.route.paramMap.subscribe(params => {
      this.products.updateProduct(params.get('commerce_id'),params.get('product_id'), this.product)
      .subscribe((data:any)=>{
        this.product = data.data
        this.product.price = this.product.price.split(" ")
        this.product.stock = this.product.stock.split(" ")
        this.updating = false
      })
    });
    this.product.price = this.product.price.split(" ")
    this.product.stock = this.product.stock.split(" ")
  }
}
