import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CommerceService } from '../../services/commerce.service';
import { NewCommerceDialogComponent } from '../../dialogs/new-commerce-dialog/new-commerce-dialog.component'
@Component({
  selector: 'app-main-view',
  templateUrl: './main-view.component.html',
  styleUrls: ['./main-view.component.scss']
})
export class MainViewComponent implements OnInit {
  
  membership
  allow_add = false
  commerces = []
  loaded = false
  constructor(
    private router: Router,
    public dialog: MatDialog,
    private route: ActivatedRoute,
    private commmerce: CommerceService
  ) { }

  ngOnInit(): void {   
    this.loaded = false
    this.getCommerces()
    this.commmerce.commerce_added.subscribe(()=>{
      this.loaded = false
      this.getCommerces()
    })
  }

  getCommerces() {
    this.commmerce.getAccountCommerce()
    .subscribe((data: any)=>{
      this.commerces = data.data
      let membership = JSON.parse(localStorage.getItem('acommerce-hash-user'))["a_commerce_membership"]
      if(membership.no_commerce > this.commerces.length){
        this.allow_add = true
      }else{ this.allow_add = false }
      this.loaded = true
    })
  }

  addNewCommerce(){
    if(this.allow_add){
      this.dialog.open(NewCommerceDialogComponent, {
        width: (window.innerWidth*.8)+'px',
        panelClass: 'popup_class'
      })
    }
  }

  checkBusiness(commerce){
    sessionStorage.setItem('a_commerce_data_info', JSON.stringify(commerce))
    this.router.navigate([commerce.id], { relativeTo: this.route })
  }

}
