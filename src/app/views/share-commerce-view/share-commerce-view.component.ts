import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { CommerceService } from '../../services/commerce.service';
import { environment } from '../../../environments/environment';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import * as jsPDF from 'jspdf';

@Component({
  selector: 'app-share-commerce-view',
  templateUrl: './share-commerce-view.component.html',
  styleUrls: ['./share-commerce-view.component.scss']
})
export class ShareCommerceViewComponent implements OnInit {
  @ViewChild('pdf') pdf: ElementRef;
  commerce_url = environment.commerce_url
  commerce_data 
  qr_code_url

  constructor(
    public location: Location, 
    private route: ActivatedRoute,
    private commerce: CommerceService
  ) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.commerce.getCommerce(params.get('commerce_id'))
      .subscribe((data:any)=>{
        console.log(data.data)
        this.commerce_data = data.data
        this.qr_code_url = this.commerce_url+encodeURI(data.data.name)
      })
    });
  }

  downloadQR(qr) {
    let canvas = qr.qrcElement.nativeElement.childNodes[0]
    let image = canvas.toDataURL()
    var a = document.createElement("a"); //Create <a>
    a.href = image; //Image Base64 Goes here
    a.download = "qr_code.png"; //File name Here
    a.click(); //Downloaded file
  }

  downloadPDF(qr) {
    var doc = new jsPDF()
    let canvas = qr.qrcElement.nativeElement.childNodes[0]
    let image = canvas.toDataURL()
    let logo = this.commerce_data.logo
    var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
    var pageWidth = doc.internal.pageSize.width || doc.internal.pageSize.getWidth();

    doc.addImage(logo,'PNG', (pageWidth / 2) - 10, 10, 20, 15);

    doc.setFontSize(26);
    doc.setTextColor(27,38,79);
    doc.text(this.commerce_data.name, pageWidth / 2, 35, "center")

    doc.setFontSize(14);
    doc.setTextColor(27,38,79);
    let text = doc.splitTextToSize(this.commerce_data.description, 180);
    doc.text(text, pageWidth / 2, 50, "center")
    
    doc.setFontSize(16);
    doc.setTextColor(27,38,79);
    doc.text("Visita nuestro comercio en línea, entrando a:", pageWidth / 2, 105, "center")

    doc.setFontSize(20);
    doc.setTextColor(27,38,79);
    doc.text(this.qr_code_url, pageWidth / 2, 115, "center")

    doc.setFontSize(18);
    doc.setTextColor(27,38,79);
    doc.text("ó", pageWidth / 2, 130, "center")

    doc.setFontSize(16);
    doc.setTextColor(27,38,79);
    doc.text("Escanea el siguiente codigo qr desde tu celular", pageWidth / 2, 145, "center")

    doc.addImage(image,'PNG', 55, 150, 100, 100);

    doc.setFillColor(27,38,79);
    doc.rect(0, pageHeight - 15, pageWidth, 160, "F");
    
    doc.setFontSize(10);
    doc.setTextColor(255);
    doc.text("A-Commerce powered by AcumaDev", pageWidth / 2, pageHeight  - 5, 'center');
    doc.save("example.pdf");
  }
}
