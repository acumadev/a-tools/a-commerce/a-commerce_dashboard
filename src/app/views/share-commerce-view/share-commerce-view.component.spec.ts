import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShareCommerceViewComponent } from './share-commerce-view.component';

describe('ShareCommerceViewComponent', () => {
  let component: ShareCommerceViewComponent;
  let fixture: ComponentFixture<ShareCommerceViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShareCommerceViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShareCommerceViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
