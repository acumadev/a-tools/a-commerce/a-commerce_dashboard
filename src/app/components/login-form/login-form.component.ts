import { LoginService } from '../../services/login.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {

  log_error = false
  constructor(
    private router: Router,
    private login: LoginService
  ) { }

  ngOnInit(): void {
  }

  loginAccount(username, password){
    this.log_error = false
    let data = { username: username.value.trim(), password: password.value.trim() }
    this.login.loginAccount(data)
    .subscribe((data:any) => {
      if(data.data.a_commerce){
        localStorage.setItem('acommerce-hash-user', JSON.stringify(data.data))
        this.router.navigate(['/commerce']);
      }else{
        alert("No tienes acceso a esta herramienta")
      }
    },()=>{this.log_error = true})
    password.value = ""
  }

}
