import { Component, OnInit } from '@angular/core';
import { CommerceService } from '../../services/commerce.service';
@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.scss']
})
export class SideNavComponent implements OnInit {

  commerces = []
  constructor(private commerce: CommerceService) { }

  ngOnInit(): void {
    this.commerce.getAccountCommerce()
    .subscribe((data:any)=>{
      this.commerces = data.data
    })
    this.commerce.commerce_added.subscribe(()=>{
      this.commerce.getAccountCommerce()
      .subscribe((data:any)=>{
        this.commerces = data.data
      })
    })
  }

}

