import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { QRCodeModule } from 'angularx-qrcode';

'Material Angular'
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { MatChipsModule } from '@angular/material/chips';
import { MatTabsModule } from '@angular/material/tabs';
import { MatSortModule } from '@angular/material/sort';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatTreeModule } from '@angular/material/tree';
import { MatCardModule } from '@angular/material/card';

import { MainViewComponent } from './views/main-view/main-view.component';
import { LoginViewComponent } from './views/login-view/login-view.component';

import { LoginFormComponent } from './components/login-form/login-form.component';
import { SideNavComponent } from './components/side-nav/side-nav.component';
import { ProductViewComponent } from './views/product-view/product-view.component';
import { AddProductComponent } from './dialogs/add-product/add-product.component';
import { CommerceViewComponent } from './views/commerce-view/commerce-view.component';
import { LayoutBuilderViewComponent } from './views/layout-builder-view/layout-builder-view.component';
import { NewCommerceDialogComponent } from './dialogs/new-commerce-dialog/new-commerce-dialog.component';
import { ShareCommerceViewComponent } from './views/share-commerce-view/share-commerce-view.component';
import { OrderViewComponent } from './views/order-view/order-view.component';
import { EditCommerceDialogComponent } from './dialogs/edit-commerce-dialog/edit-commerce-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    MainViewComponent,
    LoginViewComponent,
    LoginFormComponent,
    SideNavComponent,
    ProductViewComponent,
    AddProductComponent,
    CommerceViewComponent,
    LayoutBuilderViewComponent,
    NewCommerceDialogComponent,
    ShareCommerceViewComponent,
    OrderViewComponent,
    EditCommerceDialogComponent
  ],
  imports: [
    FormsModule,
    QRCodeModule,
    MatTabsModule,
    MatSortModule,
    MatTreeModule,
    BrowserModule,
    MatIconModule,
    MatListModule,
    MatCardModule,
    MatInputModule,
    MatChipsModule,
    MatDialogModule,
    MatSelectModule,
    MatButtonModule,
    HttpClientModule,
    MatSidenavModule,
    MatToolbarModule,
    AppRoutingModule,
    MatGridListModule,
    MatFormFieldModule,
    MatSlideToggleModule,
    BrowserAnimationsModule,
    MatProgressSpinnerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
