import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { CommerceService } from '../../services/commerce.service';
import { MainViewComponent } from '../../views/main-view/main-view.component';
@Component({
  selector: 'app-new-commerce-dialog',
  templateUrl: './new-commerce-dialog.component.html',
  styleUrls: ['./new-commerce-dialog.component.scss']
})
export class NewCommerceDialogComponent implements OnInit {

  characteristics = ['Comida','Bebida', "Carnicería", "Tocinería", "Pizzería", "Taquería"]
  delivery_service = false
  loading = false
  types = [] 
  image
  logo
  constructor(
    private commerce: CommerceService,
    public dialogRef: MatDialogRef<MainViewComponent>) { }

  ngOnInit(): void {
  }

  closePopup() {
    this.dialogRef.close();
  }

  addType(type) { 
    const index = this.types.indexOf(type.value);
    if (index < 0) {
      this.types.push(type.value);
    }
  }

  removeType(type): void {
    const index = this.types.indexOf(type);
    if (index >= 0) {
      this.types.splice(index, 1);
    }
  }

  uploadImageFile(event) {
    let reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = () => this.image = reader.result;
  }

  uploadLogoFile(event) {
    let reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = () => this.logo = reader.result;
  }

  addNewCommerce(name, description, location, number) {
    this.loading = true
    let data = {
      name:(name.value).split(" ").join("-"),
      location: location.value,
      description: description.value,
      delivery_service: this.delivery_service,
      coordinates:["0","0"],
      phone: number.value, 
      image: this.image,
      logo: this.logo,
      type: this.types,
      membership: {
        type: "free_tier",
        no_products: 10
      }
    }
    console.log(data)
    this.commerce.createCommerce(data)
    .subscribe(()=>{
      this.dialogRef.close();
    })
  }
}
