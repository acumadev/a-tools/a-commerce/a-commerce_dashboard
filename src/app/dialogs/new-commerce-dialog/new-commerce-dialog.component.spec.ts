import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewCommerceDialogComponent } from './new-commerce-dialog.component';

describe('NewCommerceDialogComponent', () => {
  let component: NewCommerceDialogComponent;
  let fixture: ComponentFixture<NewCommerceDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewCommerceDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewCommerceDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
