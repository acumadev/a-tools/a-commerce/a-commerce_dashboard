import { ProductViewComponent } from '../../views/product-view/product-view.component'
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ProductsService } from '../../services/products.service';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent implements OnInit {

  types = ["Comida", "Bebida", "Snack", "Dulces", "Papelería"]
  characteristics = []
  stock_units = ["kilogramo(s)","pieza(s)", "gramo(s)"]
  stock_unit = ""
  price_units = ["kilogramo","pieza", "gramo"]
  price_unit = ""
  selling_units = ["kilogramo(s)", "pieza(s)"]
  selling_unit = []
  product_type = ""
  commerce_id
  images = []

  loading = false

  constructor(
    private product: ProductsService,
    public dialogRef: MatDialogRef<ProductViewComponent>,
    @Inject(MAT_DIALOG_DATA) private data: any
  ) {}

  ngOnInit(){
  }

  closePopup() {
    this.dialogRef.close();
  }
  
  addUnit(type) { 
    const index = this.selling_unit.indexOf(type.value);
    if (index < 0) {
      this.selling_unit.push(type.value);
    }
  }

  removeUnit(type): void {
    const index = this.selling_unit.indexOf(type);
    if (index >= 0) {
      this.selling_unit.splice(index, 1);
    }
  }

  uploadFile(event) {
    for(let image of event.target.files){
      let reader = new FileReader();
      reader.readAsDataURL(image);
      reader.onload = () => this.images.push(reader.result);
    }
  }

  removeImage(image) {
    const index = this.images.indexOf(image);
    if (index >= 0) {
      this.images.splice(index, 1);
    }
  }

  addCharacteristic(char){
    this.characteristics.push(char.value)
    char.value = ""
  }

  removeChar(char) {
    const index = this.characteristics.indexOf(char);
    if (index >= 0) {
      this.characteristics.splice(index, 1);
    }
  }

  addProduct(name,price,stock,sku,description){
    this.loading = true
    let data = {
      sku: sku.value,
      name: name.value,
      price: price.value+" por "+this.price_unit,
      stock: stock.value+" "+this.stock_unit,
      images: this.images,
      units: this.selling_unit,
      type: this.product_type,
      description: description.value,
      characteristics: this.characteristics,
    }
    this.product.createProduct(this.data.commerce_id,data)
    .subscribe(()=>{
      this.dialogRef.close();
    })
  }

  noNegative(number) {
    // var numbers = /^[0-9]+$/;
    if(number.value < 0){
      number.value = ""
    }
  }
}

