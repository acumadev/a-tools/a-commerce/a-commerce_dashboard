import { Component, Inject, OnInit } from '@angular/core';
import { CommerceService } from '../../services/commerce.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CommerceViewComponent } from '../../views/commerce-view/commerce-view.component';

@Component({
  selector: 'app-edit-commerce-dialog',
  templateUrl: './edit-commerce-dialog.component.html',
  styleUrls: ['./edit-commerce-dialog.component.scss']
})
export class EditCommerceDialogComponent implements OnInit {

  characteristics = ['Comida','Bebida', "Carnicería", "Tocinería", "Pizzería", "Taquería"]
  commerce_info
  loading = false
  constructor(
    private commerce: CommerceService,
    public dialogRef: MatDialogRef<CommerceViewComponent>,
    @Inject(MAT_DIALOG_DATA) private data: any
  ) { }

  ngOnInit(): void {
    this.loading = true
    this.commerce.getCommerce(this.data.commerce_id)
    .subscribe((data:any)=>{
      this.commerce_info = data.data
      this.loading = false
    })
  }

  closePopup() {
    this.dialogRef.close();
  }

  addType(type) { 
    const index = this.commerce_info.types.indexOf(type.value);
    if (index < 0) {
      this.commerce_info.type.push(type.value);
    }
  }

  removeType(type): void {
    const index = this.commerce_info.types.indexOf(type);
    if (index >= 0) {
      this.commerce_info.type.splice(index, 1);
    }
  }

  uploadImageFile(event) {
    let reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = () => this.commerce_info.image = reader.result;
  }

  uploadLogoFile(event) {
    let reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = () => this.commerce_info.logo = reader.result;
  }
  
  editCommerce() {
    this.loading = true 
    this.commerce.updateCommerce(this.data.commerce_id, this.commerce_info)
    .subscribe((data)=>{
      this.dialogRef.close();
    })
  }
}
